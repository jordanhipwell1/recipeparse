//
//  RecipeCollectionViewController.m
//  RecipeTable
//
//  Created by Jordan Hipwell on 1/26/15.
//  Copyright (c) 2015 Jordan Hipwell. All rights reserved.
//

#import "RecipeCollectionViewController.h"
#import "RecipeInfoViewController.h"
#import "RecipeCollectionViewCell.h"
#import "UIColor+AppColors.h"
#import "UIImage+NetworkImages.h"

#import <Parse/Parse.h>

@interface RecipeCollectionViewController () <UICollectionViewDataSource, UICollectionViewDelegate>

@property (strong, nonatomic) NSArray *recipeData;
@property (strong, nonatomic) UICollectionView *collectionView;

@end


@implementation RecipeCollectionViewController

static NSString * RecipeDataID = @"recipeData";
static NSString * RecipeTitleID = @"recipeTitle";
static NSString * RecipeImageID = @"recipeImage";
static NSString * RecipeInstructionsID = @"recipeInstructions";

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    [self uploadRecipeData];
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    //GET request using Parse REST API
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://api.parse.com/1/classes/RecipeData/zDbWapVzaN"]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:90];
    [request addValue:@"pUsp4O5IOnhPFDz9Cl7gbJTieTFRrCNhhty2gJ40" forHTTPHeaderField:@"X-Parse-Application-Id"];
    [request addValue:@"AEqVSOBRWaOh9deufN3PakRGnHKbHsdsCaDfRBXr" forHTTPHeaderField:@"X-Parse-REST-API-Key"];
    [request setHTTPMethod:@"GET"];
    
    //TODO: Show spinner in recipe collection view
    
    NSURLSession *session = [NSURLSession sharedSession];
    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (error) {
                NSLog(@"failed to get recipe data");
                
                if (!self.recipeData || self.recipeData.count == 0) {
                    //get recipe data from plist
                    NSString *path = [[NSBundle mainBundle] pathForResource:@"Property List" ofType:@"plist"];
                    self.recipeData = [[NSArray alloc] initWithContentsOfFile:path];
                    
                    //if no recipe data (corrupt plist possibly?), get from NSUserDefaults, otherwise save to defaults
                    if (self.recipeData.count == 0) {
                        self.recipeData = [userDefaults objectForKey:RecipeDataID];
                    } else {
                        [userDefaults setObject:self.recipeData forKey:RecipeDataID];
                        [userDefaults synchronize];
                    }
                }
            } else {
                NSDictionary *JSONDict = [NSJSONSerialization JSONObjectWithData:data
                                                                         options:0
                                                                           error:nil];
                self.recipeData = JSONDict[RecipeDataID];
                
                [self.collectionView reloadData];
                
                [userDefaults setObject:self.recipeData forKey:RecipeDataID];
                [userDefaults synchronize];
            }
        });
    }] resume];
    
    //set up background
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithFrame:self.view.bounds];
    backgroundImageView.image = [UIImage imageNamed:@"wood-bg"];
    backgroundImageView.contentMode = UIViewContentModeScaleAspectFill;
    [self.view addSubview:backgroundImageView];
    
    //set up table view
    CGFloat statusBarHeight = [UIApplication sharedApplication].statusBarFrame.size.height;
    CGFloat verticalPosition = statusBarHeight + self.navigationController.navigationBar.frame.size.height;
    
    //set up collection view
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    CGFloat cellDimension = self.view.frame.size.width / 2 - 20;
    flowLayout.itemSize = CGSizeMake(cellDimension, cellDimension + 30);
    flowLayout.sectionInset = UIEdgeInsetsMake(15, 15, 15, 15);
    
    UICollectionView *collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, verticalPosition, self.view.frame.size.width, self.view.frame.size.height - verticalPosition) collectionViewLayout:flowLayout];
    collectionView.dataSource = self;
    collectionView.delegate = self;
    collectionView.backgroundColor = [UIColor clearColor];
    collectionView.alwaysBounceVertical = YES;
    [collectionView registerClass:[RecipeCollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
    
    [self.view addSubview:collectionView];
    self.collectionView = collectionView;
    
    //make nav bar transparent
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    
    //custom back button
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithTitle:@""
                                                                 style:UIBarButtonItemStylePlain
                                                                target:nil
                                                                action:nil];
    [self.navigationItem setBackBarButtonItem:backItem];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    //clear cell selection interactively upon swipe back
    if ([[self.collectionView indexPathsForSelectedItems] count] == 1) {
        [self.collectionView deselectItemAtIndexPath:[self.collectionView indexPathsForSelectedItems][0] animated:YES];
    }
    
    self.title = @"Recipes";
    UIFont *titleFont = [UIFont fontWithName:@"Zapfino" size:18];
    UIColor *titleColor = [UIColor secondaryTextColor];
    NSDictionary *titleAttributes = @{ NSFontAttributeName: titleFont,
                                       NSForegroundColorAttributeName: titleColor };
    self.navigationController.navigationBar.titleTextAttributes = titleAttributes;
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.recipeData.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    RecipeCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    cell.titleLabel.text = self.recipeData[indexPath.row][RecipeTitleID];
    cell.imageView.image = [UIImage imageFromURL:self.recipeData[indexPath.row][RecipeImageID]];
    
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    RecipeInfoViewController *recipeInfoVC = [[RecipeInfoViewController alloc] init];
    recipeInfoVC.recipeData = self.recipeData[indexPath.row];
    
    [self.navigationController pushViewController:recipeInfoVC animated:YES];
}

#pragma mark - Methods

- (void)uploadRecipeData {
    NSDictionary *rec1 = @{
                           RecipeTitleID : @"Raspberry Swirled Lemon Cheesecake Bars",
                           RecipeImageID: @"http://thebakerchick.com/wp-content/uploads/2014/02/cheesecake.jpg",
                           RecipeInstructionsID: @"Yield: 12 bars\n\nIngredients\n\nFor the Crust:\n1 1/2 cups graham cracker crumbs\n4T butter\n\nFor the Cheesecake:\n16 ounces cream cheese- room temperature\n1/2 cup sugar\n2 tablespoons lemon juice\nZest of a lemon\n4 tablespoons milk\n2 eggs- room temperature\n1 teaspoon vanilla extract\n\nFor the Raspberry Swirl:\n4 oz. of fresh raspberries (about 1/2 cup)\n2 tablespoons sugar\n\nInstructions\n  1. Preheat oven to 350F. Line an 8x8 baking pan with parchment paper, criss-crossing two rectangular pieces so there is an overhang on either side for easy lifting. Set aside.\nFor the crust\n  2. In a medium sized bowl mix together the crumbs and butter with a spoon until moist. Press into the prepared pan and bake for about 10 minutes or until crust is set around the edges.\n  3. Meanwhile make the filling:\n  4. Beat together the white sugar, and cream cheese until smooth.\n  5. Add the egg, milk, lemon juice, and vanilla and beat until smooth and creamy. Spread filling mixture over baked crust.\nFor the Raspberry Purée:\n  6. In a food processor or blender, purée the raspberries until smooth. Press through a fine mesh sieve, getting as much liquid out as possible. Discard the solids. Stir in the sugar.\n  7. Dot the raspberry mixture over the filling and use a toothpick or skewer to swirl together.\n  8. Bake for 30 minutes or until set around the edges and slightly jiggly in the center. Cool completely on a wire rack and then chill until cold before serving."
                           };
    NSDictionary *rec2 = @{
                           RecipeTitleID : @"Chocolate Fruit Cups",
                           RecipeImageID: @"http://nutritiontwins.com/wp-content/uploads/2014/02/Chocolate_cups_with_wstrawberries_.jpg",
                           RecipeInstructionsID: @"Ingredients\n1 cup chocolate chips (preferably dark chocolate one)\n10 paper cupcake liners\n2 cups fresh strawberries or any of your favorite fruit; sliced in half\nOil in a spray container (or cooking spray)\n\nInstructions\n  1. Melt chocolate in a microwave safe container (timing varies by microwave, start with 45 second intervals and depending on your microwave, see if you need more.\n  2. Lightly spray cupcake liners with oil before spooning melted chocolate inside. Carefully brush the chocolate mixture inside the liner with a spoon, creating a thin layer about ¼ inch or less. Make sure the chocolate fully covers the side of the cupcake liners.\n  3. Place on a plate in the refrigerator or freezer for 30 minutes and allow the chocolate to harden. Once hardened, check the cupcake liners to make sure there are no thin spots. If so, brush on more chocolate.\n  4. When the chocolate has hardened, peel the paper liners. You should have perfect chocolate cups. Fill each with sliced strawberries or your favorite fruit, drizzle with melted chocolate and enjoy!!"
                           };
    NSDictionary *rec3 = @{
                           RecipeTitleID : @"Salted Caramel and Chocolate Pecan Pie Bars",
                           RecipeImageID: @"http://cdn.averiecooks.com/wp-content/uploads/2014/06/pecanpiebars-9.jpg",
                           RecipeInstructionsID: @"INGREDIENTS:\n\nCrust\n1/2 cup unsalted butter, very soft (1 stick)\n1 cup all-purpose flour\n1/4 cup confectioners&apos; sugar\n1 tablespoon cornstarch\npinch salt, optional and to taste\nFilling\n8 ounces roasted salted pecans, halves or pieces okay; about 2 cups (I used Trader Joe&apos;s roasted salted halves)\n1 cup semi-sweet chocolate chips\n1/2 cup unsalted butter (1 stick)\n1 cup light brown sugar, packed\n1/3 cup whipping cream or heavy cream\n1 tablespoon vanilla extract\n1/2 teaspoon salt, or to taste\n\nDIRECTIONS:\n\n  1. Preheat oven to 350F. Line an 8-by-8-inch baking pan with aluminum foil, spray with cooking spray; set aside. I strongly urge lining the pan with foil for easier cleanup.\n  2. Crust - In a large bowl, combine all crust ingredients and using two forks or your hands (I find hands easier), cut butter into dry ingredients until evenly distributed and pea-sized lumps and sandy bits form. The softer the butter is, the quicker and easier it is.\n  3. Turn mixture out into prepared pan and pack down firmly with a spatula or hands to create an even, uniform, flat crust layer.\n  4. Filling - Evenly sprinkle the pecans.\n  5. Evenly sprinkle with the chocolate chips; set pan aside.\n  6. In a large, microwave-safe bowl, combine 1/2 cup butter, brown sugar, whipping cream, and heat on high power for 1 minute to melt.\n  7. Remove bowl from micro, and whisk until mixture is smooth; it&apos;s okay if butter hasn&apos;t completely melted.\n  8. Return bowl to microwave and heat for 1 minute on high power.\n  9. Remove bowl from micro, and whisk until mixture is smooth.\n  10. Whisk in the vanilla and salt.\n  11. Slowly and evenly pour the caramel sauce over the chocolate chips and pecans.\n  12. Place pan on a cookie sheet (as insurance against overflow) and bake for about 30 to 32 minutes, or until caramel is bubbling vigorously around edges, with bubbling to a lesser degree in center. Allow bars to cool in pan on a wire rack for at least 3 hours, or overnight (cover with a piece of foil and/or put pan inside large Ziplock), before slicing and serving. Bars will keep airtight at room temperature for up to 1 week, or in the freezer for up to 6 months."
                           };
    NSDictionary *rec4 = @{
                           RecipeTitleID : @"Lazy Ice Cream Cake",
                           RecipeImageID: @"http://allmysweetdesserts.com/wp-content/uploads/2015/01/Lazy-Ice-Cream-Cake1-e1422029632779-550x330.jpg",
                           RecipeInstructionsID: @"Ingredients\n12 ice cream sandwiches, any flavor\nIce cream, any flavor- softened\nIce cream topping, such as caramel, hot fudge, strawberry, etc.\nWhipped Topping\n\nInstructions\n  1. Line an 8\&quot; x 8\&quot; pan with saran wrap. Using half the sandwiches, make a single layer.\n  2. Cover the ice cream sandwiches with softened ice cream- try to make an even layer. (used about ⅓ of a tub of Cookies &apos;n Cream.)\n  3. Cover the ice cream with a thin layer of hot fudge\n  4. Finish the layers off with the remaining ice cream sandwiches.\n  5. Throw the pan in the fridge for about an hour so the ice cream can harden.\n  6. Once the ice cream has hardened, invert the pan onto the plate you&apos;ll serve it from.\n  7. \&quot;Frost\&quot; the cake with the whipped topping.\n  8. Try to get fancy and make lines of caramel &amp; chocolate sauce, dragging a toothpick in alternating directions to create the design.\n  9. Return the cake to the freezer until ready to serve."
                           };
    NSDictionary *rec5 = @{
                           RecipeTitleID : @"Loaded Oatmeal Chocolate Chip Cookies",
                           RecipeImageID: @"http://cdn.averiecooks.com/wp-content/uploads/2014/12/loadedoatmealcookies-5.jpg",
                           RecipeInstructionsID: @"INGREDIENTS:\n\n1 large egg\n1/2 cup unsalted butter (1 stick)\n1/2 cup light brown sugar, packed\n1/4 cup granulated sugar\n1 tablespoon vanilla extract\n1 cup old-fashioned whole rolled oats (not instant or quick cook)\n1 cup all-purpose flour\n1/2 cup sweetened shredded coconut\n1/2 cup chopped walnuts (pecans may be substituted; or nuts may be omitted)\n1/2 teaspoon baking soda\n1/4 teaspoon salt, or to taste\n1 heaping cup semi-sweet chocolate chips\n\nDIRECTIONS:\n\n  1. To the bowl of a stand mixer fitted with the paddle attachment (or large mixing bowl and electric mixer) combine the egg, butter, sugars, vanilla, and beat on medium-high speed until creamed and well combined, about 4 minutes.\n  2. Stop, scrape down the sides of the bowl, and add the oats, flour, coconut, walnuts, baking soda, salt to taste, and beat on low speed until just combined, about 1 minute.\n  3. Stop, scrape down the sides of the bowl, and add the chocolate chips, and beat on low speed until just combined, about 30 seconds.\n  4. Using a large cookie scoop, 1/4-cup measure, or your hands, form approximately 12 equal-sized mounds of dough, roll into balls, and flatten slightly. Tip - Strategically place a few chocolate chips on top of each mound of dough by taking chips from the underside and adding them on top.\n  5. Place mounds on a large plate or tray, cover with plasticwrap, and refrigerate for at least 2 hours, up to 5 days. Do not bake with unchilled dough because cookies will bake thinner, flatter, and be more prone to spreading.\n  6. Preheat oven to 350F, line a baking sheet with a Silpat or spray with cooking spray. Place dough mounds on baking sheet, spaced at least 2 inches apart (I bake 8 cookies per sheet) and bake for about 11 t0 13 minutes (for super soft cookies, longer for more well-done cookies), or until edges have set and tops are just set, even if slightly undercooked, pale, and glossy in the center; don&apos;t overbake. Cookies firm up as they cool. Allow cookies to cool on baking sheet for about 10 minutes before serving. I let them cool on the baking sheet and don&apos;t use a rack.\n  7. Cookies will keep airtight at room temperature for up to 1 week or in the freezer for up to 6 months. Alternatively, unbaked cookie dough can be stored in an airtight container in the refrigerator for up to 5 days, or in the freezer for up to 4 months, so consider baking only as many cookies as desired and save the remaining dough to be baked in the future when desired."
                           };

    NSArray *arrayOfRecipes = @[rec1, rec2, rec3, rec4, rec5];
    NSDictionary *recipeDataDict = @{ @"recipeData" : arrayOfRecipes };

    PFObject *testObject = [PFObject objectWithClassName:@"RecipeData" dictionary:recipeDataDict];
    [testObject saveInBackground];
}


//DEL REQUEST
//    NSURL* url = [NSURL URLWithString:@"https://api.parse.com/1/classes/TestClass/1xaLSQjB1O"];
//
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:90];
//    [request addValue:@"oVAxJh2ir0U5QIUvGZx2AlHwv8ol6V8Y2D2BcoLU" forHTTPHeaderField:@"X-Parse-Application-Id"];
//    [request addValue:@"QXQwxzBeTby55ZGZJDjxpsNQDrrnijF64gfmzRZx" forHTTPHeaderField:@"X-Parse-REST-API-Key"];
//    [request setHTTPMethod:@"DELETE"];
//
//    __autoreleasing NSURLResponse  *response = nil;
//
//    NSError *errorObj;
//
//    NSData *data= [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&errorObj];
//
//    NSString *responseString =   [[NSString alloc] initWithBytes:[data bytes] length:[data length] encoding:NSUTF8StringEncoding];
//
//    NSLog(@"%@", responseString);


//GET ALL OBJECTS
//    NSURL* url = [NSURL URLWithString:@"https://api.parse.com/1/classes/TestClass"];
//
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:90];
//    [request addValue:@"oVAxJh2ir0U5QIUvGZx2AlHwv8ol6V8Y2D2BcoLU" forHTTPHeaderField:@"X-Parse-Application-Id"];
//    [request addValue:@"QXQwxzBeTby55ZGZJDjxpsNQDrrnijF64gfmzRZx" forHTTPHeaderField:@"X-Parse-REST-API-Key"];
//    [request setHTTPMethod:@"GET"];
//
//    __autoreleasing NSURLResponse  *response = nil;
//
//    NSError *errorObj;
//
//    NSData *data= [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&errorObj];
//
//    NSString *responseString =   [[NSString alloc] initWithBytes:[data bytes] length:[data length] encoding:NSUTF8StringEncoding];
//
//    NSLog(@"%@", responseString);


//PUT REQUEST
//    NSURL* url = [NSURL URLWithString:@"https://api.parse.com/1/classes/TestClass/5mqxY2W3la"];
//
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:90];
//    [request addValue:@"oVAxJh2ir0U5QIUvGZx2AlHwv8ol6V8Y2D2BcoLU" forHTTPHeaderField:@"X-Parse-Application-Id"];
//    [request addValue:@"QXQwxzBeTby55ZGZJDjxpsNQDrrnijF64gfmzRZx" forHTTPHeaderField:@"X-Parse-REST-API-Key"];
//    [request setHTTPMethod:@"PUT"];
//
//    NSDictionary* requestBody = @{@"imageName":@"this changed"};
//
//    if (requestBody) {
//        NSError *error;
//        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:requestBody
//                                                           options:0
//
//                                                             error:&error];
//        NSString* json;
//
//        if (!error) {
//            json = [[NSString alloc]initWithData:jsonData encoding:NSUTF8StringEncoding];
//        }
//
//        NSMutableData *d = [NSMutableData dataWithData:[json dataUsingEncoding:NSUTF8StringEncoding]];
//        [request setHTTPBody:d];
//    }
//
//
//    __autoreleasing NSURLResponse  *response = nil;
//
//    // Synchronous requests wrap the async call in a blocking thread. If
//    // authentication fails, the connection attempts to continue. We lose
//    // the error code at this point because instead of being "authentication
//    // failed" it becomes "user cancelled auth request"
//    NSError *errorObj;
//    NSData *data= [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&errorObj];
//
//    NSString *responseString =   [[NSString alloc] initWithBytes:[data bytes] length:[data length] encoding:NSUTF8StringEncoding];
//
//    NSLog(@"%@", responseString);


//GET REQUEST
//    NSURL* url = [NSURL URLWithString:@"https://api.parse.com/1/classes/TestClass/5mqxY2W3la"];
//
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:90];
//    [request addValue:@"oVAxJh2ir0U5QIUvGZx2AlHwv8ol6V8Y2D2BcoLU" forHTTPHeaderField:@"X-Parse-Application-Id"];
//    [request addValue:@"QXQwxzBeTby55ZGZJDjxpsNQDrrnijF64gfmzRZx" forHTTPHeaderField:@"X-Parse-REST-API-Key"];
//    [request setHTTPMethod:@"GET"];
//
//    __autoreleasing NSURLResponse  *response = nil;
//
//    NSError *errorObj;
//
//    NSData *data= [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&errorObj];
//
//    NSString *responseString =   [[NSString alloc] initWithBytes:[data bytes] length:[data length] encoding:NSUTF8StringEncoding];
//
//    NSLog(@"%@", responseString);


//POST REQUEST
//    NSURL* url = [NSURL URLWithString:@"https://api.parse.com/1/classes/TestClass"];
//
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:90];
//    [request addValue:@"oVAxJh2ir0U5QIUvGZx2AlHwv8ol6V8Y2D2BcoLU" forHTTPHeaderField:@"X-Parse-Application-Id"];
//    [request addValue:@"QXQwxzBeTby55ZGZJDjxpsNQDrrnijF64gfmzRZx" forHTTPHeaderField:@"X-Parse-REST-API-Key"];
//    [request setHTTPMethod:@"POST"];
//
//    NSDictionary* requestBody = @{@"imageName":@"blabla"};
//
//    if (requestBody) {
//        NSError *error;
//        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:requestBody
//                                                           options:0
//
//                                                             error:&error];
//        NSString* json;
//
//        if (!error) {
//            json = [[NSString alloc]initWithData:jsonData encoding:NSUTF8StringEncoding];
//        }
//
//        NSMutableData *d = [NSMutableData dataWithData:[json dataUsingEncoding:NSUTF8StringEncoding]];
//        [request setHTTPBody:d];
//    }
//
//
//    __autoreleasing NSURLResponse  *response = nil;
//
//    // Synchronous requests wrap the async call in a blocking thread. If
//    // authentication fails, the connection attempts to continue. We lose
//    // the error code at this point because instead of being "authentication
//    // failed" it becomes "user cancelled auth request"
//    NSError *errorObj;
//    NSData *data= [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&errorObj];
//
//    NSString *responseString =   [[NSString alloc] initWithBytes:[data bytes] length:[data length] encoding:NSUTF8StringEncoding];
//
//    NSLog(@"%@", responseString);


//JSON File
//    NSURL* url = [NSURL URLWithString:@"http://aasquaredapps.com/TheMTPit/TheMTPITJSON.json"];
//
//    NSURLRequest* request = [[NSURLRequest alloc]initWithURL:url];
//
//    NSData *theData = [NSURLConnection sendSynchronousRequest:request
//                                            returningResponse:nil
//                                                        error:nil];
//
//    NSArray *newJSON = [NSJSONSerialization JSONObjectWithData:theData
//                                                            options:0
//                                                              error:nil];
//
//
//
//    for (int i = 0; i < newJSON.count; i++) {
//        NSDictionary* dictionary = [newJSON objectAtIndex:i];
//        NSLog(@"%@", [dictionary objectForKey:@"organization"]);
//    }


//Parse stuff
//    PFQuery *query = [PFQuery queryWithClassName:@"TestClass"];
//    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
//        if (!error) {
//            // The find succeeded.
//            NSLog(@"Successfully retrieved %lu scores.", (unsigned long)objects.count);
//            // Do something with the found objects
//            for (PFObject *object in objects) {
//                [self displayImageFromObject:object];
//
//                LibraryBookObject* obj = [[LibraryBookObject alloc]initWithDictionary:object];
//                NSLog(@"%@",obj.bookName);
//            }
//
//
//
//        } else {
//            // Log details of the failure
//            NSLog(@"Error: %@ %@", error, [error userInfo]);
//        }
//    }];

//    PFObject *testObject = [PFObject objectWithClassName:@"TestObject"];
//    testObject[@"foo"] = @"bar";
//    [testObject saveInBackground];


@end
