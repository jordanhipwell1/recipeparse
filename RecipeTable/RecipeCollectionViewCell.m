//
//  RecipeCollectionViewCell.m
//  RecipeTable
//
//  Created by Jordan Hipwell on 2/25/15.
//  Copyright (c) 2015 Jordan Hipwell. All rights reserved.
//

#import "RecipeCollectionViewCell.h"
#import "UIColor+AppColors.h"

@implementation RecipeCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self initialize];
    }
    
    return self;
}

- (void)initialize {
    self.backgroundColor = [UIColor clearColor];
    
    //selected background view
    UIView *selectedView = [[UIView alloc] init];
    selectedView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    selectedView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.4];
    self.selectedBackgroundView = selectedView;
    
    //recipe image
    CGFloat padding = 2;
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(padding, padding, self.frame.size.width - padding * 2, self.frame.size.height - padding * 2 - 30)];
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    imageView.clipsToBounds = YES;
    [self.contentView addSubview:imageView];
    self.imageView = imageView;
    
    //recipe title
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(imageView.frame.origin.x, imageView.frame.origin.y + imageView.frame.size.height + 5, imageView.frame.size.width, 25)];
    label.numberOfLines = 1;
    label.font = [UIFont fontWithName:@"Papyrus" size:[UIFont preferredFontForTextStyle:UIFontTextStyleCaption1].pointSize];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor primaryTextColor];
    [self.contentView addSubview:label];
    self.titleLabel = label;
}

@end
