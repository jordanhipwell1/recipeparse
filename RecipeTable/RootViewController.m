//
//  RootViewController.m
//  RecipeTable
//
//  Created by Jordan Hipwell on 1/28/15.
//  Copyright (c) 2015 Jordan Hipwell. All rights reserved.
//

#import "RootViewController.h"
#import "RecipeCollectionViewController.h"

@interface RootViewController ()

@end

@implementation RootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    RecipeCollectionViewController *vc = [[RecipeCollectionViewController alloc] init];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:vc];
    [self addChildViewController:navController];
    [self.view addSubview:navController.view];
}

@end
