//
//  RecipeInfoViewController.m
//  RecipeTable
//
//  Created by Jordan Hipwell on 1/28/15.
//  Copyright (c) 2015 Jordan Hipwell. All rights reserved.
//

#import "RecipeInfoViewController.h"
#import "UIColor+AppColors.h"
#import "UIImage+NetworkImages.h"

@interface RecipeInfoViewController ()

@end

@implementation RecipeInfoViewController

static NSString * RecipeTitleID = @"recipeTitle";
static NSString * RecipeImageID = @"recipeImage";
static NSString * RecipeInstructionsID = @"recipeInstructions";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //show recipe image full screen
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:[UIImage imageFromURL:self.recipeData[RecipeImageID]]];
    backgroundImageView.contentMode = UIViewContentModeScaleAspectFill;
    backgroundImageView.clipsToBounds = YES;
    backgroundImageView.frame = self.view.bounds;
    [self.view addSubview:backgroundImageView];
    
    //add blur above image
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    UIVisualEffectView *effectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    effectView.frame = self.view.bounds;
    [backgroundImageView addSubview:effectView];
    
    //recipe image
    CGFloat statusBarHeight = [UIApplication sharedApplication].statusBarFrame.size.height;
    CGFloat verticalPosition = statusBarHeight + self.navigationController.navigationBar.frame.size.height;
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageFromURL:self.recipeData[RecipeImageID]]];
    imageView.frame = CGRectMake(0, verticalPosition, self.view.frame.size.width, self.view.frame.size.height / 3.25);
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    imageView.clipsToBounds = YES;
    [self.view addSubview:imageView];
    
    //recipe instructions
    UITextView *instructionsTextView = [[UITextView alloc] initWithFrame:CGRectMake(0, imageView.frame.origin.y + imageView.frame.size.height, self.view.frame.size.width, self.view.frame.size.height - (imageView.frame.origin.y + imageView.frame.size.height))];
    instructionsTextView.editable = NO;
    instructionsTextView.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.05]; //must be non-clear to allow touch interaction
    instructionsTextView.font = [UIFont systemFontOfSize:14];
    NSString *instructions = [self.recipeData[RecipeInstructionsID] stringByReplacingOccurrencesOfString:@"\\n" withString:@"\n"];
    instructionsTextView.text = instructions;
    instructionsTextView.textContainerInset = UIEdgeInsetsMake(20, 20, 20, 20);
    [self.view addSubview:instructionsTextView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    //recipe title
    self.title = self.recipeData[RecipeTitleID];
    UIFont *titleFont = [UIFont fontWithName:@"Papyrus" size:14];
    UIColor *titleColor = [UIColor primaryTextColor];
    NSDictionary *titleAttributes = @{ NSFontAttributeName: titleFont,
                                       NSForegroundColorAttributeName: titleColor };
    self.navigationController.navigationBar.titleTextAttributes = titleAttributes;
}

@end
