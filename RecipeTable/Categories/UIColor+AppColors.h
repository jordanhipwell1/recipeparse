//
//  UIColor+AppColors.h
//  RecipeTable
//
//  Created by Jordan Hipwell on 3/15/15.
//  Copyright (c) 2015 Jordan Hipwell. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (AppColors)

+ (UIColor *)primaryTextColor;
+ (UIColor *)secondaryTextColor;

@end
