//
//  UIColor+AppColors.m
//  RecipeTable
//
//  Created by Jordan Hipwell on 3/15/15.
//  Copyright (c) 2015 Jordan Hipwell. All rights reserved.
//

#import "UIColor+AppColors.h"

@implementation UIColor (AppColors)

+ (UIColor *)primaryTextColor {
    return [UIColor blackColor];
}

+ (UIColor *)secondaryTextColor {
    return [[UIColor blackColor] colorWithAlphaComponent:0.65];
}

@end
