//
//  UIImage+NetworkImages.m
//  RecipeTable
//
//  Created by Jordan Hipwell on 3/15/15.
//  Copyright (c) 2015 Jordan Hipwell. All rights reserved.
//

#import "UIImage+NetworkImages.h"

@implementation UIImage (NetworkImages)

+ (UIImage *)imageFromURL:(NSString *)urlString {
    NSURL *url = [NSURL URLWithString:urlString];
    NSData *data = [NSData dataWithContentsOfURL:url]; //TODO: Replace with with a non-blocking request
    
    return [[UIImage alloc] initWithData:data];
}

@end
